#!/usr/bin/python3
# -*- coding: utf-8 -*-

import argparse
import json
import os
import sys
import traceback

import allzweckmesser as azm


def main(hypotactic_dir, top_out_dir, meters=['hexameter']):
    corpus = azm.corpus.HypotacticCorpus.from_directory(hypotactic_dir)
    for document in corpus.documents:
        print('Processing {}'.format(document.title))
        try:
            doc_out_dir = os.path.join(top_out_dir, document.title)
            os.makedirs(doc_out_dir, exist_ok=True)
            for meter in meters:
                verses_for_meter = [
                    azm.corpus.HypotacticLine(line).verse.to_dict()
                    for line in document.get_lines_with_meter([meter])
                ]
                if verses_for_meter:
                    with open(os.path.join(doc_out_dir,
                                           '{}.json'.format(meter)),
                              'w') as f:
                        json.dump(verses_for_meter, f)
        except Exception:
            print('ERROR at document {}'.format(document.title),
                  file=sys.stderr)
            traceback.print_exc()


def parse_args_and_main():
    d = 'Extract lines from a Hypotactic corpus'
    parser = argparse.ArgumentParser(description=d)
    parser.add_argument('--meters', '-m', nargs='+',
                        help='Meters of the lines to extract')
    parser.add_argument('hypotactic_dir',
                        help='Top level directory of the Hypotactic corpus')
    parser.add_argument('top_out_dir',
                        help='Top level directory of the created JSON files')
    args = parser.parse_args()
    main(**vars(args))


if __name__ == '__main__':
    parse_args_and_main()
