#!/usr/bin/python3
# -*- coding: utf-8 -*-

import argparse
import json
import os
import sys
import traceback

import allzweckmesser as azm


def main(corpus_specification, top_in_dir, top_out_dir):
    spec = json.load(open(corpus_specification))
    os.makedirs(top_out_dir, exist_ok=True)
    for split in ['train', 'dev', 'test']:
        print('Split {}'.format(split))
        split_file = os.path.join(top_out_dir, '{}.json'.format(split))
        instances = []
        for meter_name, paths in spec[split].items():
            print('Processing meter {}'.format(meter_name))
            meter = azm.meters.ALL_METERS[meter_name]
            for path in paths:
                print('  Processing path {}'.format(path))
                with open(os.path.join(top_in_dir, path)) as f:
                    verses = []
                    for v_dict in json.load(f):
                        try:
                            verse = azm.model.Verse.from_json(v_dict)
                        except Exception:
                            print('ERROR in verse {!r}'.format(v_dict),
                                  file=sys.stderr)
                            traceback.print_exc()
                            verse = None
                        if verse:
                            verses.append(verse)
                for verse in verses:
                    print('    Processing verse {}'.format(verse.text))
                    matches = bool(meter.match_reading(verse.readings[0]))
                    instances.append([meter_name, verse.to_json(), matches])
        with open(split_file, 'w') as f:
            json.dump(instances, f)


def parse_args_and_main():
    d = 'Create a train-dev-test split corpus from a corpus specfication'
    parser = argparse.ArgumentParser(description=d)
    parser.add_argument('corpus_specification',
                        help='JSON specifying the splits and their files')
    parser.add_argument('top_in_dir',
                        help='Top level directory of the extracted json files')
    parser.add_argument('top_out_dir',
                        help='Top level directory of the created JSON files')
    args = parser.parse_args()
    main(**vars(args))


if __name__ == '__main__':
    parse_args_and_main()
