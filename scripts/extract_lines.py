#!/usr/bin/python3
# -*- coding: utf-8 -*-

import argparse
import itertools

import allzweckmesser as azm


def main(hypotactic_dir, outfile, limit=0, title='Lines', meters=tuple()):
    corpus = azm.corpus.HypotacticCorpus.from_directory(hypotactic_dir)
    line_generator = corpus.get_lines_with_meter(meters)
    if limit:
        line_generator = itertools.islice(line_generator, limit)
    with open(outfile, 'w') as f:
        corpus.save_html_tags(f, line_generator, title=title)


def parse_args_and_main():
    d = 'Extract lines from a Hypotactic corpus'
    parser = argparse.ArgumentParser(description=d)
    parser.add_argument('--limit', '-l', type=int, default=0,
                        help='Maximum number of lines to extract')
    parser.add_argument('--meters', '-m', nargs='+',
                        help='Meters of the lines to extract')
    parser.add_argument('--title', '-t', default='Lines',
                        help='Title of the resulting HTML document')
    parser.add_argument('hypotactic_dir',
                        help='Top level directory of the Hypotactic corpus')
    parser.add_argument('outfile', help='File to save the lines in')
    args = parser.parse_args()
    main(**vars(args))


if __name__ == '__main__':
    parse_args_and_main()
