# Allzweckmesser

A Tool for Measuring Latin Verse

# Installation

For development, install the package with the `--editable` option. If
you only want to use the package, you can omit this option.

```
pip install --editable /path/to/Allzweckmesser  # The repo dir, not the package dir
```

## Test installation

If the package was installed correctly, you should be able to get
usage information when calling the package with the `-h` option:

```
python -m allzweckmesser -h
```

## Running Tests

When you’re in the repository root, you can run the unit tests like this:

```
python /path/to/setup.py test
```

A better way is to install `pytest` and `pytest-env` and then run the
test using the `py.test` executable directly:

```
pytest
```

The advantage of this is that the test requirements (`pytest` and
`pytest-env`) don’t have to be downloaded every time the tests are
executed.

# Notes
* Gladius by parkjisun from the Noun Project

## Interface

 - text
 - u/v richtig oder unbekannt
 - Altlatein
 - Menge an erlaubten Metren
 - Autor
