import os.path
from setuptools import find_packages, setup


def in_this_dir(basename):
    return os.path.join(os.path.dirname(__file__), basename)

with open(in_this_dir('README.md')) as f:
    LONG_DESCRIPTION = f.read()

with open(in_this_dir('requirements.txt')) as f:
    REQUIREMENTS = [line.strip() for line in f
                    if not line.strip().startswith('#')]

print(REQUIREMENTS)
setup(
    name='allzweckmesser',
    version='0.0.1dev',
    packages=find_packages(),
    long_description=LONG_DESCRIPTION,
    include_package_data=True,
    install_requires=REQUIREMENTS,
    setup_requires=['pytest-runner'],
    tests_require=['pytest', 'pytest-env'],
)
