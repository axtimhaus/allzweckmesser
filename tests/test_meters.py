# -*- coding: utf-8 -*-

import os.path
import re

import pytest

import allzweckmesser as azm

TEST_DIR = os.path.dirname(__file__)
RE_MATCH_TYPE = type(re.match('a', 'a'))


@pytest.fixture
def verse_models():
    verse_filepath = os.path.join(TEST_DIR, 'verses.json')
    verse_models = azm.model.from_json(verse_filepath)
    return verse_models


@pytest.fixture
def pentameter_verse(verse_models):
    return verse_models[2]


@pytest.fixture
def hexameter_verse(verse_models):
    return verse_models[3]


@pytest.fixture
def hexameter():
    return azm.meters.ALL_METERS['Catalectic Dactylic Hexameter']


def test_caesura_together(hexameter, hexameter_verse):
    reading = hexameter_verse.readings[0]
    penthemimeral_reward = azm.meters.caesurae_together(
        [('mora', 10, 'Penthemimeral')], 1)
    assert penthemimeral_reward(hexameter, reading) == 1

    trithemimeral_reward = azm.meters.caesurae_together(
        [('mora', 6, 'Trithemimeral')], 1
    )
    assert trithemimeral_reward(hexameter, reading) == 0

    hephthemimeral_reward = azm.meters.caesurae_together(
        [('mora', 14, 'Hephthemimeral')], 1
    )
    assert hephthemimeral_reward(hexameter, reading) == 1

    trit_and_hepht_hemimeral_reward = azm.meters.caesurae_together(
        [('mora', 6, 'Hephthemimeral'), ('mora', 14, 'Hephthemimeral')], 1
    )
    assert trit_and_hepht_hemimeral_reward(hexameter, reading) == 0


def test_match_reading_success(hexameter, hexameter_verse):
    match = hexameter.match_reading(hexameter_verse.readings[0])
    assert isinstance(match, RE_MATCH_TYPE)


def test_match_reading_fail(hexameter, pentameter_verse):
    match = hexameter.match_reading(pentameter_verse.readings[0])
    assert match is None
