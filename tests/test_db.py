# -*- coding: utf-8 -*-

import allzweckmesser


def test_get_db_uri_sqlite():
    config = {'dialect': 'sqlite', 'file': '/tmp.db'}
    uri = allzweckmesser.db.get_db_uri(config)
    assert uri == 'sqlite:////tmp.db'


def test_get_db_uri_postgresql():
    config = {'dialect': 'postgresql', 'user': 'Creutzer',
              'password': 'Günderode', 'host': 'localhost',
              'port': 1234, 'database': 'skph'}
    uri = allzweckmesser.db.get_db_uri(config)
    assert uri == 'postgresql://Creutzer:Günderode@localhost:1234/skph'


def test_get_db_uri_postgresql_no_port():
    config = {'dialect': 'postgresql', 'user': 'Creutzer',
              'password': 'Günderode', 'host': 'skph.de',
              'database': 'skph'}
    uri = allzweckmesser.db.get_db_uri(config)
    assert uri == 'postgresql://Creutzer:Günderode@skph.de/skph'
