# -*- coding: utf-8 -*-

import os

import pytest
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

import allzweckmesser as azm
FormAnalysis = azm.db.FormAnalysis

TEST_DIR = os.path.dirname(__file__)
MACRONS_FILE = os.path.join(TEST_DIR, 'test_macrons.txt')
NONEXISTENT_WORD = 'nonexistent_word'

DB_CONFIG = {'dialect': 'sqlite', 'file': ':memory:'}
ENGINE = create_engine(azm.db.get_db_uri(DB_CONFIG))
SESSION_FACTORY = sessionmaker(bind=ENGINE)
azm.db.BASE.metadata.create_all(ENGINE)


@pytest.fixture
def word_list():
    word_list = azm.wordlist.WordList(session_factory=SESSION_FACTORY)
    return word_list


@pytest.fixture
def populated_word_list(word_list):
    word_list.populate_database(MACRONS_FILE)
    yield word_list
    word_list.depopulate_database()


@pytest.fixture
def loaded_word_list(populated_word_list):
    populated_word_list.load_all_from_db()
    populated_word_list.unknown_forms.add(NONEXISTENT_WORD)
    return populated_word_list


# Shorthands
pwl = populated_word_list
lwl = loaded_word_list


def test_populate_database(word_list):
    word_list.populate_database(MACRONS_FILE)
    analyses = word_list._session.query(FormAnalysis).all()
    assert len(analyses) == 6
    zephyrus_l = [a for a in analyses if a.lemma == 'Zephyrus']
    assert len(zephyrus_l) == 1
    zephyrus = zephyrus_l[0]
    assert isinstance(zephyrus, FormAnalysis)
    assert zephyrus.form == 'zephyrus'
    assert zephyrus.morphtag == 'n-s---mn-'
    assert zephyrus.accented == 'Ze^phy^rus'
    word_list.depopulate_database()


def test_depopulate_database(pwl):
    pwl.depopulate_database()
    analyses = pwl._session.query(FormAnalysis).all()
    assert len(analyses) == 0


def test_delete_duplicates_from_db(pwl):
    analyses = pwl._session.query(FormAnalysis).all()
    assert len(analyses) == 6
    for a in analyses:
        copied_analysis = FormAnalysis(
            form=a.form, morphtag=a.morphtag,
            lemma=a.lemma, accented=a.accented
        )
        pwl._session.add(copied_analysis)
    pwl._session.commit()
    analyses = pwl._session.query(FormAnalysis).all()
    assert len(analyses) == 12
    pwl._delete_duplicates_from_db()
    analyses = pwl._session.query(FormAnalysis).all()
    assert len(analyses) == 6


def test_load_from_db_success(pwl):
    analyses = pwl.load_from_db('abluentium')
    assert isinstance(analyses, set)
    assert all(isinstance(a, FormAnalysis) for a in analyses)
    assert len(analyses) == 2
    assert all(a.form == 'abluentium' for a in analyses)
    assert all(a.lemma == 'abluo' for a in analyses)
    assert all(a.accented == 'ablu^entium' for a in analyses)
    assert {a.morphtag for a in analyses} == {'v-pppamg-', 'v-pppang-'}

    assert len(pwl.form_analyses) == 1
    assert len(pwl.unknown_forms) == 0
    assert 'abluentium' in pwl.form_analyses


def test_load_from_db_fail(pwl):
    analyses = pwl.load_from_db(NONEXISTENT_WORD)
    assert isinstance(analyses, set)
    assert not analyses
    assert len(pwl.form_analyses) == 0
    assert len(pwl.unknown_forms) == 0


def test_get_morphtags_success(lwl):
    tags = lwl.get_morphtags('abluentium')
    assert isinstance(tags, set)
    assert tags == {'v-pppamg-', 'v-pppang-'}


def test_get_morphtags_fail(lwl):
    tags = lwl.get_morphtags(NONEXISTENT_WORD)
    assert isinstance(tags, set)
    assert not tags


def test_get_lemmas_success(lwl):
    lemmas = lwl.get_lemmas('abluentium')
    assert isinstance(lemmas, set)
    assert lemmas == {'abluo'}


def test_get_lemmas_fail(lwl):
    lemmas = lwl.get_lemmas(NONEXISTENT_WORD)
    assert isinstance(lemmas, set)
    assert not lemmas


def test_get_accenteds_success(lwl):
    accenteds = lwl.get_accenteds('abluentium')
    assert isinstance(accenteds, set)
    assert accenteds == {'ablu^entium'}


def test_get_accenteds_fail(lwl):
    accenteds = lwl.get_accenteds(NONEXISTENT_WORD)
    assert isinstance(accenteds, set)
    assert not accenteds


@pytest.mark.skipif(not os.path.isdir(azm.config.MORPHEUS_DIR),
                    reason='MORPHEUS_DIR does not exist')
def test_analyze_with_morpheus_single_word(word_list):
    analyses = word_list.analyze_with_morpheus(['aeris'])
    assert len(analyses['aeris']) == 6


@pytest.mark.skipif(not os.path.isdir(azm.config.MORPHEUS_DIR),
                    reason='MORPHEUS_DIR does not exist')
def test_analyze_with_morpheus_multi_word(word_list):
    analyses = word_list.analyze_with_morpheus(['aeris', 'deinde', 'res'])
    assert analyses


@pytest.mark.skipif(not os.path.isdir(azm.config.MORPHEUS_DIR),
                    reason='MORPHEUS_DIR does not exist')
def test_analyze(word_list):
    analyses = word_list.analyze('regina')
    assert len(analyses) == 3
    assert all(isinstance(a, FormAnalysis) for a in analyses)


def test_cache_analyses(lwl):
    assert len(lwl.form_analyses['abluentium']) == 2
    abluentium_analyses = {
        FormAnalysis(form='abluentium', morphtag='v-pppamg-',
                     lemma='abluo', accented='ablu^entium'),
        FormAnalysis(form='abluentium', morphtag='v-pppang-',
                     lemma='abluo', accented='ablu^entium'),
        FormAnalysis(form='abluentium', morphtag='v-pppafg-',
                     lemma='abluo', accented='ablu^entium')
    }
    lwl.cache_analyses({'abluentium': abluentium_analyses})
    assert len(lwl.form_analyses['abluentium']) == 3

    assert len(lwl.form_analyses['ancilla']) == 0
    lwl.unknown_forms.add('ancilla')
    assert 'ancilla' in lwl.unknown_forms
    ancilla_analyses = {
        FormAnalysis(form='ancilla', morphtag='n-s---fb-',
                     lemma='ancilla', accented='ancilla_'),
        FormAnalysis(form='ancilla', morphtag='n-s---fn-',
                     lemma='ancilla', accented='ancilla'),
        FormAnalysis(form='ancilla', morphtag='n-s---fv-',
                     lemma='ancilla', accented='ancilla')
    }
    lwl.cache_analyses({'ancilla': ancilla_analyses})
    assert len(lwl.form_analyses['ancilla']) == 3
    assert 'ancilla' not in lwl.unknown_forms
