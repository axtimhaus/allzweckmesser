# -*- coding: utf-8 -*-

from bs4 import BeautifulSoup
import pytest

import allzweckmesser as azm


@pytest.fixture
def aratea_element():
    # The line is:
    # 'vertitur: [hanc nemo certo tibi dicere possit,'
    return BeautifulSoup(
        '<div class="line"><span class="word"><span class="syll'
        ' long">ver</span><span class="syll short">ti</span><span'
        ' class="syll short">tur:</span></span><span class="word"><span'
        ' class="syll long">[hanc</span></span><span class="word"><span'
        ' class="syll long">nē</span><span class="syll'
        ' long">mō</span></span><span class="word"><span class="syll'
        ' long">cer</span><span class="syll long">tō</span></span><span'
        ' class="word"><span class="syll short">ti</span><span class="syll'
        ' short">bi</span></span><span class="word"><span class="syll'
        ' long">dī</span><span class="syll short">ce</span><span class="syll'
        ' short">re</span></span><span class="word"><span class="syll'
        ' long">pos</span><span class="syll long">sit,</span></span></div>',
        'lxml'
    )


def test_get_reading_from_line_element(aratea_element):
    reading = azm.corpus.get_reading_from_line_element(aratea_element)
    verse = 'vertitur: [hanc nemo certo tibi dicere possit,'

    assert len(reading.tokens) == 7

    assert reading.tokens[0].text == 'vertitur:'
    assert reading.tokens[1].text == '[hanc'
    assert reading.tokens[2].text == 'nēmō'
    assert reading.tokens[3].text == 'certō'
    assert reading.tokens[4].text == 'tibi'
    assert reading.tokens[5].text == 'dīcere'
    assert reading.tokens[6].text == 'possit,'

    assert reading.tokens[0].span == [0, 9]
    assert reading.tokens[1].span == [10, 15]
    assert reading.tokens[2].span == [16, 20]
    assert reading.tokens[3].span == [21, 26]
    assert reading.tokens[4].span == [27, 31]
    assert reading.tokens[5].span == [32, 38]
    assert reading.tokens[6].span == [39, 46]

    assert len(reading.tokens[0].syllables) == 3
    assert reading.tokens[0].syllables[0].text == 'ver'
    assert reading.tokens[0].syllables[1].text == 'ti'
    assert reading.tokens[0].syllables[2].text == 'tur:'


def test_separate_punctuation(aratea_element):
    reading = azm.corpus.get_reading_from_line_element(aratea_element)
    reading.tokens = azm.corpus.separate_punctuation(reading.tokens)
    verse = 'vertitur: [hanc nemo certo tibi dicere possit,'

    assert len(reading.tokens) == 10

    assert reading.tokens[0].text == 'vertitur'
    assert reading.tokens[1].text == ':'
    assert reading.tokens[2].text == '['
    assert reading.tokens[3].text == 'hanc'
    assert reading.tokens[4].text == 'nēmō'
    assert reading.tokens[5].text == 'certō'
    assert reading.tokens[6].text == 'tibi'
    assert reading.tokens[7].text == 'dīcere'
    assert reading.tokens[8].text == 'possit'
    assert reading.tokens[9].text == ','

    assert reading.tokens[0].span == [0, 8]
    assert reading.tokens[1].span == [8, 9]
    assert reading.tokens[2].span == [10, 11]
    assert reading.tokens[3].span == [11, 15]
    assert reading.tokens[4].span == [16, 20]
    assert reading.tokens[5].span == [21, 26]
    assert reading.tokens[6].span == [27, 31]
    assert reading.tokens[7].span == [32, 38]
    assert reading.tokens[8].span == [39, 45]
    assert reading.tokens[9].span == [45, 46]

    assert len(reading.tokens[0].syllables) == 3
    assert reading.tokens[0].syllables[0].text == 'ver'
    assert reading.tokens[0].syllables[0].span == [0, 3]
    assert reading.tokens[0].syllables[1].text == 'ti'
    assert reading.tokens[0].syllables[1].span == [3, 5]
    assert reading.tokens[0].syllables[2].text == 'tur'
    assert reading.tokens[0].syllables[2].span == [5, 8]

    assert reading.tokens[1].syllables == []
    assert reading.tokens[2].syllables == []

    assert len(reading.tokens[3].syllables) == 1
    assert reading.tokens[3].syllables[0].text == 'hanc'
    assert reading.tokens[3].syllables[0].span == [11, 15]

    assert len(reading.tokens[4].syllables) == 2
    assert reading.tokens[4].syllables[0].text == 'nē'
    assert reading.tokens[4].syllables[0].span == [16, 18]
    assert reading.tokens[4].syllables[1].text == 'mō'
    assert reading.tokens[4].syllables[1].span == [18, 20]
