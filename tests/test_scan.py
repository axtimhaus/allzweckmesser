# -*- coding: utf-8 -*-

import os.path

import allzweckmesser

TEST_DIR = os.path.dirname(__file__)


def test_get_plain_verses():
    correct = [
        'nunc dum tibi lubet licetque pota perde rem',
        'antehac est habitus parcus nec magis continens',
        "clamavit moriens lingua: 'Corinna, vale!'",
        'an, quod ubique, tuum est? tua sunt Heliconia Tempe?',
    ]
    test_infile = os.path.join(TEST_DIR, 'verses.txt')
    plain_verses = allzweckmesser.scan.get_plain_verses(test_infile)
    assert plain_verses == correct
