# -*- coding: utf-8 -*-

import logging
import re
import os.path
import sys
import traceback

from bs4 import BeautifulSoup
from unidecode import unidecode

from .model import Reading, Syllable, Token, Verse

BASE_HTML = """<!DOCTYPE html PUBLIC"-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta content="text/html; charset=utf-8" http-equiv="Content-type" >
</head>
<body>
</body>
</html>
"""


def get_reading_from_line_element(element):
    tokens = []
    span_begin = 0
    idx = 0
    for token_tag in element.find_all(name='span', class_='word'):
        syllables = []
        token_text = token_tag.text
        token = Token(
            token=unidecode(token_text),
            span=[span_begin, span_begin + len(token_text)]
        )

        for syllable_tag in token_tag.find_all(name='span', class_='syll'):
            syllable_text = syllable_tag.text
            if 'long' in syllable_tag.attrs['class']:
                syllable_length = 2
            elif 'short' in syllable_tag.attrs['class']:
                syllable_length = 1
            elif 'elided' in syllable_tag.attrs['class']:
                syllable_length = 0
            else:
                raise ValueError(
                    'Could not determine syllable length of syllable {!r}'
                    .format(syllable_tag)
                )
            syllable = Syllable(
                idx=idx,
                syllable=unidecode(syllable_text),
                span=[span_begin, span_begin + len(syllable_text)],
                syllable_length=syllable_length,
                vowel_length=None
            )
            idx += 1
            syllables.append(syllable)
            span_begin += len(syllable_text)
        # The + 1 is for simulating a space between tokens.
        span_begin += 1

        token.syllables = syllables
        tokens.append(token)

    return Reading(tokens=tokens)


def separate_punctuation(tokens):
    i = 0
    while i < len(tokens):
        token = tokens[i]
        m = re.match(r'^(?P<pre_punct>[\W_]*)(?P<non_punct>\w*)'
                     '(?P<post_punct>[\W_]*)$',
                     token.text)
        if m:
            pre = m.group('pre_punct')
            post = m.group('post_punct')

            # Create tokens for the punctuation before a token.
            span_begin = token.span[0]
            for c in pre:
                tokens.insert(i, Token(c, [span_begin, span_begin + 1]))
                span_begin += 1
                i += 1

            # Create tokens for the punctuation after a token.
            span_begin = token.span[1] - len(post)
            for c in m.group('post_punct'):
                tokens.insert(i + 1,
                              Token(c, [span_begin, span_begin + 1]))
                span_begin += 1
                i += 1

            # Remove the punctuation from the original token and
            # from its syllables.
            token.text = m.group('non_punct')
            span_begin = token.span[0] + m.start('non_punct')
            span_end = token.span[1] - len(post)
            token.span = [span_begin, span_end]
            if pre:
                token.syllables[0].text = token.syllables[0].text[len(pre):]
                token.syllables[0].span[0] = span_begin
            if post:
                token.syllables[-1].text = (token.syllables[-1].
                                            text[:-len(post)])
                token.syllables[-1].span[1] = span_end

        else:
            logging.warn('{!r} does not match the punctuation regex.'
                         .format(token))
        i += 1

    return tokens


def reconstruct_verse_text_from_reading(reading):
    try:
        codepoints = [' ' for _ in range(reading.tokens[-1].span[1])]
        for token in reading.tokens:
            codepoints[token.span[0]:token.span[1]] = token.text
    except Exception:
        print('ERROR reconstructing verse from reading {!r}'
              .format(reading), file=sys.stderr)
        traceback.print_exc()
        codepoints = []
    return ''.join(codepoints)


class HypotacticLine:

    def __init__(self, element):
        self.element = element
        reading = get_reading_from_line_element(element)
        reading.tokens = separate_punctuation(reading.tokens)
        text = reconstruct_verse_text_from_reading(reading)
        self.verse = Verse(verse=text, readings=[reading])


class HypotacticDocument:

    def __init__(self, file_path, parser='lxml'):
        with open(file_path) as f:
            try:
                self.root = BeautifulSoup(f, parser)
                self.title = self.root.title.text
            except Exception as e:
                print('Exception {!r} when parsing file {!r}'
                      .format(e, file_path))
                self.title = None

    def get_poems(self, filters=tuple()):
        yield from (
            poem
            for poem in self.root.find_all(name='div', class_='poem')
            if all(fil(poem) for fil in filters)
        )

    def get_lines(self, line_filters=tuple()):
        yield from (
            line
            for line in self.root.find_all(name='div', class_='line')
            if all(fil(line) for fil in line_filters)
        )

    def get_lines_with_meter(self, meters):
        filters = [lambda tag: any((meter in tag.attrs['class'])
                                   for meter in meters)]
        if self.root.find(name='div', class_='poem'):
            yield from (
                line
                for poem in self.get_poems(filters)
                for line in poem.find_all(name='div', class_='line')
            )
        else:
            self.get_lines(filters)


class HypotacticCorpus:

    def __init__(self, file_paths, parser='lxml'):
        self.file_paths = file_paths
        self.parser = parser
        self.documents = [HypotacticDocument(p, parser=parser)
                          for p in file_paths]

    @classmethod
    def from_directory(cls, directory, *args, **kwargs):
        file_paths = [os.path.abspath(os.path.join(directory, basename))
                      for basename in os.listdir(directory)]
        return cls(file_paths, *args, **kwargs)

    def get_poems(self, filters=tuple()):
        yield from (
            poem
            for doc in self.documents
            for poem in doc.get_poems(filters)
        )

    def get_lines(self, line_filters=tuple()):
        yield from (
            line
            for doc in self.documents
            for line in doc.get_lines(line_filters)
        )

    def get_lines_with_meter(self, meters):
        yield from (
            line
            for doc in self.documents
            for line in doc.get_lines_with_meter(meters)
        )

    def save_html_tags(self, file_handle, tags, title='Saved Poems',
                   base_html=BASE_HTML, pretty=False):
        soup = BeautifulSoup(base_html, self.parser)

        title_tag = soup.new_tag('title')
        title_tag.string = title
        soup.find(name='head').append(title_tag)

        latin = soup.new_tag('div')
        latin.attrs['class'] = 'latin'
        for tag in tags:
            latin.append(tag)
        soup.find(name='body').append(latin)

        if pretty:
            output = soup.prettify()
        else:
            output = str(soup)
        file_handle.write(output)
