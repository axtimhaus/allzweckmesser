# -*- coding: utf-8 -*-

from colorama import init, Back, Fore, Style

init()


def mark_long(text):
    return ('{Style.BRIGHT}{text}{Style.NORMAL}'
            .format(Style=Style, text=text))


def mark_wrong_length(text):
    return ('{Fore.RED}{text}{Fore.RESET}'
            .format(Fore=Fore, text=text))


def mark_wrong_syllables(text):
    return ('{Back.RED}{text}{Back.RESET}'
            .format(Back=Back, text=text))


def mark_correct(text):
    return ('{Fore.GREEN}{text}{Fore.RESET}'
            .format(Fore=Fore, text=text))


def mark_syllables_provider(text, provider):
    if provider == 'get_syllables_for_accented_form':
        return text
    elif provider == 'SPECIAL_CASES':
        return '{}{}'.format(text, '₀')
    elif provider == 'get_syllables_for_unknown_form':
        return '{}{}'.format(text, '₁')
    else:
        return '{}{}'.format(text, 'ₑ')
