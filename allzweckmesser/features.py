# -*- coding: utf-8 -*-

from enum import Enum


class ReadingFeature(Enum):
    MCL_TRIGGERS_PL = 0
    SYNIZESIS = 1
    S_ELISION = 2
    HIAT = 3


class ReadingMeterFeatures(Enum):
    DOES_NOT_FIT_METER = 10
    NECESSARY_CHANGES_TO_MAKE_IT_FIT = 11
    METER = 12
    NO_USUAL_BREAK_PRESENT = 13
    HEXAMETER_BRIDGE_VIOLATED = 14


class CombinedFeatures(Enum):
    # The values have to start at 0 and be contiguous.
    MCL_TRIGGERS_PL = 0
    SYNIZESIS = 1
    DOES_NOT_FIT_METER = 2
    NO_USUAL_BREAK_PRESENT = 3
    METER_RULES_VIOLATED = 4


def combine_features(reading_features, reading_meter_features):
    features = [0 for _ in CombinedFeatures]
    for rf, val in reading_features.items():
        if hasattr(CombinedFeatures, rf.name):
            features[CombinedFeatures[rf.name].value] = val

    meter_rules_violated = 0
    for rmf, val in reading_meter_features.items():
        if hasattr(CombinedFeatures, rmf.name):
            features[CombinedFeatures[rmf.name].value] = val
        elif 'VIOLATED' in rmf.name.upper():
            meter_rules_violated += 1
    features[
        CombinedFeatures.METER_RULES_VIOLATED.value] = meter_rules_violated

    return features
