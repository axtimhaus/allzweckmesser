#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import sys
from sklearn.externals import joblib
from typing import List

from .config import RANKING_MODEL_PATH
from .features import combine_features
from .meters import ALL_METERS, ALL_METER_NAMES, get_reading_meter_combinations
from .model import Verse
from .scanner import Scanner


def scan(plain_verses: List[str], meters=ALL_METER_NAMES,
         **options) -> List[Verse]:
    """Scan Latin verses."""
    meters = [ALL_METERS[m] for m in meters if m in ALL_METERS]
    scanner = Scanner()
    scanned_verses = scanner.scan_verses(plain_verses)
    model = joblib.load(RANKING_MODEL_PATH)
    for verse in scanned_verses:
        reading_meter_combinations = (
            get_reading_meter_combinations(
                verse.readings, meters
            )
        )
        vectors = []
        for reading, meter, rmfeatures in reading_meter_combinations:
            reading.meter = meter
            vectors.append(combine_features(reading.features, rmfeatures))
        probs = model.predict_proba(vectors)
        sorted_probs = sorted(
            [(probs[i], reading) for i in range(len(probs))],
            key=lambda x: x[0][0]
        )
        first_one = sorted_probs[:1]
        verse.readings = [prob_reading[1] for prob_reading in first_one]
    return scanned_verses


def parse_args() -> argparse.Namespace:
    """Parse arguments from the commandline.

    :return: An argparse Namespace holding the arguments.
    """
    d = 'Scan Latin verses.'
    parser = argparse.ArgumentParser(prog='allzweckmesser', description=d)
    parser.add_argument('--infile', help=('A file containing the verses that'
                                          ' are to be scanned.'))
    parser.add_argument('--meters', '-m', nargs='+', help=('The considered'
                                                           ' meters.'))
    args = parser.parse_args()
    return args


def get_plain_verses(infile: str = None) -> List[str]:
    """Read verses that are to be scanned.

    If infile is None the verses are read from the standard input.

    :params infile: A file containing one verse per line.
    :return: A list of the verses.
    """
    if infile:
        with open(infile) as f:
            plain_verses = [line.strip() for line in f.readlines()]
    else:
        plain_verses = [line.strip() for line in sys.stdin.readlines()]
    return plain_verses


def main():
    """Parse CLI arguments then read and scan verses."""
    args = vars(parse_args())
    args['plain_verses'] = get_plain_verses(args['infile'])
    del args['infile']
    scanned_verses = scan(**args)
    for v in scanned_verses:
        print(v)


if __name__ == '__main__':
    main()
