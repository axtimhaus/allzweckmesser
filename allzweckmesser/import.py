#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import argparse
import sys
from model import *

def parse_args() -> argparse.Namespace:
    """Parse arguments from the commandline.
    
    :return: An argparse Namespace holding the arguments.
    """
    d = 'Import annotated verses from .json file.'
    parser = argparse.ArgumentParser(prog='allzweckmesser', description=d)
    parser.add_argument('--json',
        help='A file containing the verses that are to be imported.')
    args = parser.parse_args()
    return args


def json_from_file(filename:str)->list:
    """Convert json-file to python object
    
    :return: .json-file as python3 object
    """
    with open(filename, 'r') as json_file:
        return json.loads(json_file.read())
    



    
if __name__ == 'main':
    args = parse_args()
