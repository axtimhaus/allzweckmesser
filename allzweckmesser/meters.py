#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import itertools
import re

from .features import ReadingMeterFeatures
from .model import Reading, Position


def bridge(position_spec, feature):
    def get_feature(meter: Meter, reading: Reading):
        position = Position.after(position_spec[0], reading, meter,
                                  position_spec[1])
        if position and position.word_boundary:
            return None
        else:
            return feature
    return get_feature


class Meter:

    def __init__(self, name: str, schema: str, breaks: list = None,
                 conditions: list = None, short_name: str = None,
                 id: int = None):
        self.name = name
        self.schema = schema
        self.break_specs = breaks
        # Convert condition functions to instance-bound methods.
        self.conditions = ([cond.__get__(self) for cond in conditions]
                           if conditions else [])
        self.short_name = short_name
        self.id = id

    def match_reading(self, reading: Reading):
        return re.match(self.schema, reading.get_schema())

    def collect_condition_features(self, reading: Reading):
        features = []
        for cond in self.conditions:
            feature = cond(reading)
            if feature:
                features.append(feature)
        return features

    def reading_has_usual_breaks(self, reading: Reading):
        if self.break_specs:
            for breaks in self.break_specs:
                satisfied = True
                for b in breaks:
                    position = Position.after(b[0], reading, b[1], self)
                    if not (hasattr(position, 'word_boundary')
                            and position.word_boundary):
                        satisfied = False
                        break
                if satisfied:
                    return True
            else:
                return False
        else:
            return True


AEOLIC_BASE = r'(?:(–)(–)|(–)(⏑)|(⏑)(–))'

ALL_METERS = {
    'hexameter': Meter(
        'Catalectic Dactylic Hexameter',
        r'(–)(⏑⏑|–)(–)(⏑⏑|–)(–)(⏑⏑|–)(–)(⏑⏑|–)(–)(⏑⏑|–)(⏑|–)',
        conditions=[
            bridge(('mora', 15, 'Hermann’s Bridge'),
                   ReadingMeterFeatures.HEXAMETER_BRIDGE_VIOLATED)
        ],
        breaks=[
            [('mora', 6, 'Trithemimeral'), ('mora', 14, 'Hephthemimeral')],
            [('mora', 10, 'Penthemimeral')],
            [('mora', 16, 'Bucolic Diaeresis')]
        ],
        short_name='hexameter',
        id=0
    ),
    'pentameter': Meter(
        'Dactylic Pentameter',
        r'(–)(⏑⏑|–)(–)(⏑⏑|–)(–)(–)(⏑⏑)(–)(⏑⏑)(⏑|–)',
        breaks=[[('mora', 5, 'Middle diaeresis')]],
        short_name='pentameter',
        id=1
    ),
    'ia6': Meter(
        'Iambic Trimeter',
        r'(⏑|⏑⏑|–)(⏑⏑|–)(⏑)(⏑⏑|–)(⏑|⏑⏑|–)(⏑⏑|–)(⏑)(⏑⏑|–)(⏑|⏑⏑|–)(⏑⏑|–)(⏑)(⏑|–)',
        breaks=[
            [('element', 4, 'After fourth element')],
            [('element', 8, 'After eighth element')]
        ],
        short_name='ia6',
        id=2
    ),
    'senarii': Meter(
        'Iambic Senarius',
        r'(⏑|⏑⏑|–)(⏑⏑|–)(⏑|⏑⏑|–)(⏑⏑|–)(⏑|⏑⏑|–)(⏑⏑|–)(⏑|⏑⏑|–)(⏑⏑|–)(⏑|⏑⏑|–)(⏑⏑|–)(⏑)(⏑|–)',
        short_name='senarii',
        id=3
    ),
    'sap hen': Meter(
        'Sapphic Hendecasyllable',
        r'(–)(–|⏑)(–)(–|⏑)(–)(⏑)(⏑)(–)(⏑)(–)(⏑|–)',
        conditions={},
        short_name='sap hen',
        id=4
    ),
    'adoneus': Meter(
        'Adoneus',
        r'(–)(⏑⏑)(–)(⏑|–)',
        short_name='adoneus',
        id=5
    ),
    'hendecasyllables': Meter(
        'Phalaecian Hendecasyllable',
        AEOLIC_BASE + r'(–)(⏑)(⏑)(–)(⏑)(–)(⏑)(–)(⏑|–)',
        breaks=[[('syllable', 6, 'After sixth syllable')]],
        short_name='hendecasyllables',
        id=6
    ),
    'scazon': Meter(
        'Choliamb',
        r'(⏑|–)(–)(⏑)(–)(⏑|–)(–)(⏑)(–)(⏑)(–)(–)(⏑|–)',
        breaks=[[('syllable', 5, 'After fifth syllable')],
                [('syllable', 7, 'After seventh syllable')]],
        short_name='scazon',
        id=7
    ),
}

ALL_METER_NAMES = list(ALL_METERS.keys())


def get_reading_meter_combinations(readings, meters=ALL_METERS):
    reading_meter_rmfeatures = [
        [reading, meter, {}]
        for reading, meter
        in itertools.product(readings, meters)
    ]
    for reading, meter, rmfeatures in reading_meter_rmfeatures:
        rmfeatures[ReadingMeterFeatures.DOES_NOT_FIT_METER] = int(
            meter.match_reading(reading) is None)

        # XXX: Implement this.
        rmfeatures[ReadingMeterFeatures.NECESSARY_CHANGES_TO_MAKE_IT_FIT] = 0

        rmfeatures[ReadingMeterFeatures.METER] = meter.id
        rmfeatures[ReadingMeterFeatures.NO_USUAL_BREAK_PRESENT] = int(
            meter.reading_has_usual_breaks(reading))
        for feature in meter.collect_condition_features(reading):
            rmfeatures[feature] = 1
    return reading_meter_rmfeatures
