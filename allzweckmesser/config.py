import os

ROOT = os.path.dirname(os.path.dirname(__file__))

DATABASE = {
    'dialect': 'sqlite',
    'file': os.path.join(ROOT, 'azm.db')
}

MACRONS_FILE = os.environ.get('MACRONS_FILE',
                              os.path.join(ROOT, 'macrons.txt'))
MORPHEUS_DIR = os.environ.get('MORPHEUS_DIR',
                              os.path.join(ROOT, 'morpheus'))
MODE = os.environ.get('AZM_MODE', 'run')

POPULATE_DATABASE = os.environ.get('AZM_POPULATE_DATABASE', False)

RANKING_MODEL_PATH = os.environ.get('RANKING_MODEL_PATH',
                                    os.path.join(ROOT,
                                                 'tree_classifier.joblib'))
